<?php
/**
 * =============================================================================
 * Copyright (c) 2014-2015, Kirill Stepanov
 * All rights reserved.
 *
 * This file is part of MetaNotes and is licensed by the Copyright holder under
 * the 3-clause BSD License. The full text of the license can be found in the
 * LICENSE.txt file included in the root directory of this distribution or at
 * the link below.
 * =============================================================================
 *
 * @license http://opensource.org/licenses/BSD-3-Clause
 */

namespace MetaNotes;

use ReflectionClass;
use InvalidArgumentException;
use ArrayAccess,
    IteratorAggregate,
    Countable;

class AnnotatedClass
extends ReflectionClass
implements ArrayAccess, IteratorAggregate, Countable
{
    use AbstractAnnotatedClass;


    public function __construct($val)
    {
        if($val instanceof ReflectionClass)
        {
            $val = $val->name;
        }
        else if(is_object($val))
        {
            $val = get_class($val);
        }
        else if(!is_string($val))
        {
            throw new InvalidArgumentException('Parameter is not a reflection, a class or a string.');
        }

        if(!class_exists($val) && !interface_exists($val) && !trait_exists($val))
        {
            throw new InvalidArgumentException('Class(/interface/reflection) does not exist: ' . $val);
        }

        parent::__construct($val);
        $this->setAnnotations($this->getDocComment());
        $this->setDefaultFactory();
    }
}
