<?php
/**
 * =============================================================================
 * Copyright (c) 2014-2015, Kirill Stepanov
 * All rights reserved.
 *
 * This file is part of MetaNotes and is licensed by the Copyright holder under
 * the 3-clause BSD License. The full text of the license can be found in the
 * LICENSE.txt file included in the root directory of this distribution or at
 * the link below.
 * =============================================================================
 *
 * @license http://opensource.org/licenses/BSD-3-Clause
 */

namespace MetaNotes;

class CachedAnnotationFactory implements IAnnotationFactory
{
    private $_factory;
    private $_classes = array();
    private $_functions = array();
    private $_methods = array();
    private $_objects = array();
    private $_parameters = array();
    private $_properties = array();


    public function __construct()
    {
        $this->_factory = new DefaultAnnotationFactory;
    }


    public function getClass($class)
    {
        if($class instanceof \ReflectionClass)
        {
            $class = $class->name;
        }

        if(!isset($this->_classes[$class]))
        {
            $this->_classes[$class] = $this->_factory->getClass($class)->setFactory($this);
        }

        return $this->_classes[$class];
    }


    public function getFunction($function)
    {
        // closures cannot be serialized
        if($function instanceof \Closure)
        {
            return $this->_factory->getFunction($function)->setFactory($this);
        }

        if($function instanceof \ReflectionFunction)
        {
            $function = $function->name;
        }


        if(!isset($this->_functions[$function]))
        {
            $tmp = $this->_factory->getFunction($function)->setFactory($this);
            $this->_functions[$function] = $tmp;
        }

        return $this->_functions[$function];
    }


    public function getMethod($class, $method=null)
    {
        $key = $class . '#' . $method;

        if(!isset($this->_methods[$key]))
        {
            $tmp = $this->_factory->getMethod($class, $method)->setFactory($this);
            $this->_methods[$key] = $tmp;
        }

        return $this->_methods[$key];
    }


    public function getObject($object)
    {
        $key = get_class($object);

        if(!isset($this->_objects[$key]))
        {
            $tmp = $this->_factory->getObject($object)->setFactory($this);
            $this->_objects[$key] = $tmp;
        }

        return $this->_objects[$key];
    }


    public function getParameter($function, $parameter)
    {
        if(is_string($function))
        {
            $key = $function . '#' . $parameter;
        }
        else if($function instanceof \ReflectionFunction)
        {
            $key = $function->name . '#' . $parameter;
        }
        else if($function instanceof \ReflectionMethod)
        {
            $key = $function->class . '*' . $function->name . '#' . $parameter;
        }
        else if(is_array($function))
        {
            $key = implode('*', $function) . '#' . $parameter;
        }
        else
        {
            throw new \InvalidArgumentException;
        }

        if(!isset($this->_parameters[$key]))
        {
            $tmp = $this->_factory->getParameter($function, $parameter)->setFactory($this);
            $this->_parameters[$key] = $tmp;
        }

        return $this->_parameters[$key];
    }


    public function getProperty($class, $property)
    {
        $key = $class . '#' . $property;

        if(!isset($this->_properties[$key]))
        {
            $tmp = $this->_factory->getProperty($class, $property)->setFactory($this);
            $this->_properties[$key] = $tmp;
        }

        return $this->_properties[$key];
    }
}
