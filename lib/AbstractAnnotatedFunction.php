<?php
/**
 * =============================================================================
 * Copyright (c) 2014-2015, Kirill Stepanov
 * All rights reserved.
 *
 * This file is part of MetaNotes and is licensed by the Copyright holder under
 * the 3-clause BSD License. The full text of the license can be found in the
 * LICENSE.txt file included in the root directory of this distribution or at
 * the link below.
 * =============================================================================
 *
 * @license http://opensource.org/licenses/BSD-3-Clause
 */

namespace MetaNotes;

trait AbstractAnnotatedFunction
{
    public function getParameters()
    {
        $output = array();

        foreach(parent::getParameters() as $reflection)
        {
            $output[] = $this->_factory->getParameter($this, $reflection->name);
        }

        return $output;
    }
}
