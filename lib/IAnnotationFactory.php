<?php
/**
 * =============================================================================
 * Copyright (c) 2014-2015, Kirill Stepanov
 * All rights reserved.
 *
 * This file is part of MetaNotes and is licensed by the Copyright holder under
 * the 3-clause BSD License. The full text of the license can be found in the
 * LICENSE.txt file included in the root directory of this distribution or at
 * the link below.
 * =============================================================================
 *
 * @license http://opensource.org/licenses/BSD-3-Clause
 */

namespace MetaNotes;


interface IAnnotationFactory
{
    /**
     * Creates a new AnnotatedClass for the given class.
     *
     *  @param      $class      mixed
     *  @return     AnnotatedClass
     */
    public function getClass($class);


    /**
     * Creates a new AnnotatedFunction for the given function.
     *
     * @param       $function       mixed
     * @return      AnnotatedFunction
     */
    public function getFunction($function);


    /**
     * Creates a new AnnotatedMethod for the given method.
     *
     * @param       $class      mixed
     * @param       $method     mixed
     * @return      AnnotatedMethod
     */
    public function getMethod($class, $method=null);


    /**
     * Creates a new AnnotatedObject for the given object.
     *
     * @param       $object     mixed
     * @return      AnnotatedObject
     */
    public function getObject($object);


    /**
     * Creates a new AnnotatedParameter for the given parameter.
     *
     * @param       $function   mixed
     * @param       $parameter  mixed
     * @return      AnnotatedParameter
     */
    public function getParameter($function, $parameter);


    /**
     * Creates a new AnnotatedProperty for the given property.
     *
     * @param       $class      mixed
     * @param       $property   mixed
     * @return      AnnotatedProperty
     */
    public function getProperty($class, $property);
}
