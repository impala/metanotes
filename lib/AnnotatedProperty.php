<?php
/**
 * =============================================================================
 * Copyright (c) 2014-2015, Kirill Stepanov
 * All rights reserved.
 *
 * This file is part of MetaNotes and is licensed by the Copyright holder under
 * the 3-clause BSD License. The full text of the license can be found in the
 * LICENSE.txt file included in the root directory of this distribution or at
 * the link below.
 * =============================================================================
 *
 * @license http://opensource.org/licenses/BSD-3-Clause
 */

namespace MetaNotes;

use ReflectionProperty;
use ArrayAccess,
    IteratorAggregate,
    Countable;

class AnnotatedProperty
extends ReflectionProperty
implements ArrayAccess, IteratorAggregate, Countable
{
    use Annotations;


    public function __construct($class, $name=null)
    {
        if($class instanceof ReflectionProperty)
        {
            parent::__construct($class->class, $class->name);
        }
        else if($name !== null)
        {
            parent::__construct($class, $name);
        }
        else
        {
            throw new \InvalidArgumentException('Please provide a property name.');
        }

        $this->setAnnotations($this->getDocComment());
        $this->setDefaultFactory();
    }


    public function getDeclaringClass()
    {
        $class_reflection = parent::getDeclaringClass();
        return $this->_factory->getClass($class_reflection);
    }
}
