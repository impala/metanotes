<?php
/**
 * =============================================================================
 * Copyright (c) 2014-2015, Kirill Stepanov
 * All rights reserved.
 *
 * This file is part of MetaNotes and is licensed by the Copyright holder under
 * the 3-clause BSD License. The full text of the license can be found in the
 * LICENSE.txt file included in the root directory of this distribution or at
 * the link below.
 * =============================================================================
 *
 * @license http://opensource.org/licenses/BSD-3-Clause
 */

namespace MetaNotes;

use ReflectionMethod;
use ArrayAccess,
    IteratorAggregate,
    Countable;

class AnnotatedMethod
extends ReflectionMethod
implements ArrayAccess, IteratorAggregate, Countable
{
    use Annotations, AbstractAnnotatedFunction;


    public function __construct($class, $name=null)
    {
        if($name)
        {
            parent::__construct($class, $name);
        }
        else if($class instanceof ReflectionMethod)
        {
            parent::__construct($class->class, $class->name);
        }
        else
        {
            parent::__construct($class);
        }

        $this->setAnnotations($this->getDocComment());
        $this->setDefaultFactory();
    }


    public function getDeclaringClass()
    {
        return $this->_factory->getClass($this->class);
    }


    public function getPrototype()
    {
        $prototype_reflection = parent::getPrototype();
        return $this->_factory->getMethod(
            $prototype_reflection->class, $prototype_reflection->name);
    }
}
