<?php
/**
 * =============================================================================
 * Copyright (c) 2014-2015, Kirill Stepanov
 * All rights reserved.
 *
 * This file is part of MetaNotes and is licensed by the Copyright holder under
 * the 3-clause BSD License. The full text of the license can be found in the
 * LICENSE.txt file included in the root directory of this distribution or at
 * the link below.
 * =============================================================================
 *
 * @license http://opensource.org/licenses/BSD-3-Clause
 */

namespace MetaNotes;

use ReflectionParameter,
    ReflectionMethod,
    ReflectionFunction;
use ArrayAccess,
    IteratorAggregate,
    Countable;

class AnnotatedParameter
extends ReflectionParameter
implements ArrayAccess, IteratorAggregate, Countable
{
    use Annotations;


    public function __construct($function, $name=null)
    {
        if($name !== null)
        {
            if($function instanceof \ReflectionFunction)
            {
                $function = $function->name;
            }
            else if($function instanceof \ReflectionMethod)
            {
                $function = [$function->class, $function->name];
            }

            parent::__construct($function, $name);
        }
        else if($function instanceof ReflectionParameter)
        {
            $function_reflection = $function->getDeclaringFunction();

            if($function_reflection instanceof ReflectionMethod)
            {
                $class_reflection = $function_reflection->getDeclaringClass();
                $identifier = [$class_reflection->name, $function_reflection->name];
            }
            else
            {
                $identifier = $function_reflection->name;
            }

            parent::__construct($identifier, $function->name);
        }
        else
        {
            throw new \InvalidArgumentException('Parameter is not a reflection or a parameter.');
        }

        // parameters cannot have doc strings
        $this->setAnnotations(false);
        $this->setDefaultFactory();
    }


    public function getClass()
    {
        $class_reflection = parent::getClass();
        return $class_reflection
            ? $this->_factory->getClass($class_reflection)
            : null;
    }


    public function getDeclaringClass()
    {
        $class_reflection = parent::getDeclaringClass();
        return $class_reflection
            ? $this->_factory->getClass($class_reflection)
            : null;
    }


    public function getDeclaringFunction()
    {
        $function_reflection = parent::getDeclaringFunction();

        if($function_reflection instanceof ReflectionMethod)
        {
            return $this->_factory->getMethod(
                $function_reflection->class,
                $function_reflection->name)
            ->setFactory($this->_factory);
        }
        else
        {
            return $this->_factory->getFunction($function_reflection->name);
        }
    }
}
