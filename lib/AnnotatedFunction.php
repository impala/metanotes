<?php
/**
 * =============================================================================
 * Copyright (c) 2014-2015, Kirill Stepanov
 * All rights reserved.
 *
 * This file is part of MetaNotes and is licensed by the Copyright holder under
 * the 3-clause BSD License. The full text of the license can be found in the
 * LICENSE.txt file included in the root directory of this distribution or at
 * the link below.
 * =============================================================================
 *
 * @license http://opensource.org/licenses/BSD-3-Clause
 */

namespace MetaNotes;

use ReflectionFunction;
use ArrayAccess,
    IteratorAggregate,
    Countable;

class AnnotatedFunction
extends ReflectionFunction
implements ArrayAccess, IteratorAggregate, Countable
{
    use Annotations, AbstractAnnotatedFunction;


    public function __construct($name)
    {
        if($name instanceof ReflectionFunction)
        {
            parent::__construct($name->name);
        }
        else
        {
            try
            {
                parent::__construct($name);
            }
            catch(\ReflectionException $e)
            {
                throw new \InvalidArgumentException($e->getMessage(), $e->getCode(), $e);
            }
        }

        $this->setAnnotations($this->getDocComment());
        $this->setDefaultFactory();
    }


    public function getClosureScopeClass()
    {
        $class_reflection = parent::getClosureScopeClass();
        return $class_reflection
            ? $this->_factory->getClass($class_reflection)
            : null;
    }
}
