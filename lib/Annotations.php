<?php
/**
 * =============================================================================
 * Copyright (c) 2013-2014, Philip Graham
 * Copyright (c) 2014-2015, Kirill Stepanov
 * All rights reserved.
 *
 * This file is part of MetaNotes and is licensed by the Copyright holder under
 * the 3-clause BSD License. The full text of the license can be found in the
 * LICENSE.txt file included in the root directory of this distribution or at
 * the link below.
 * =============================================================================
 *
 * @license http://opensource.org/licenses/BSD-3-Clause
 */

namespace MetaNotes;

Annotations::$default_factory = new DefaultAnnotationFactory;


/**
 * This class parses a given Reflector for annotations and provides array style
 * or method style access to them.  Only Reflectors that implement the
 * getDocComment method are supported.
 */
trait Annotations
{
    /*
     * The reflection element's annotations as returned by
     * ReflectionHelper::getAnnotations().
     */
    private $_annotations = array();

    private $_has_doc_string;

    private $_factory;

    public static $default_factory;


    protected function setDefaultFactory()
    {
        $this->_factory = Annotations::$default_factory;
    }


    public function setFactory(IAnnotationFactory $factory)
    {
        $this->_factory = $factory;
        return $this;
    }


    /**
     * Returns all annotations as an array.
     * Note: the case of the keys is not folded, but as defined in the original
     * docstring instead.
     *
     * @return      array
     */
    public function asArray()
    {
        return $this->_annotations;
    }


    /**
     * Returns true if the parser was able to load a doctring, false otherwise.
     * If you're sure that your docstring is properly formed, make sure that
     * your OpCache isn't stripping comments.
     *
     * @return      bool
     */
    public function hasDocString()
    {
        return $this->_has_doc_string;
    }

    /*
     * ===========================================================================
     * ArrayAccess implementation
     * ===========================================================================
     */


    /**
     * Whether or not the requested annotation exists.
     *
     * @param mixed $offset
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->_annotations[strtolower($offset)]);
    }


    /**
     * Get the information for the requested annotation.
     *
     * @param mixed $offset
     * @return mixed
     */
    public function offsetGet($offset)
    {
        $offset = strtolower($offset);
        return isset($this[$offset]) ? $this->_annotations[$offset] : null;
    }


    /**
     * Set the given annotation to be the given value.
     *
     * @param mixed $offset
     * @param mixed $value
     */
    public function offsetSet($offset, $value)
    {
        throw new \LogicException('Annotations are immutable.');
    }


    /**
     * Unset the annotation at the given offset.
     *
     * @param mixed $offset
     */
    public function offsetUnset($offset)
    {
        throw new \LogicException('Annotations are immutable');
    }


    /*
     * ===========================================================================
     * IteratorAggregate implementation
     * ===========================================================================
     */

    public function getIterator()
    {
        return new \ArrayIterator($this->_annotations);
    }


    /*
     * ===========================================================================
     * Countable implementation
     * ===========================================================================
     */

    public function count()
    {
        return count($this->_annotations);
    }


    private function setAnnotations($comment)
    {
        if($comment === false)
        {
            $this->_annotations = array();
            $this->_has_doc_string = false;
        }
        else
        {
            $this->_annotations = AnnotationParser::getAnnotations($comment);
            $this->_has_doc_string = true;
        }
    }
}
