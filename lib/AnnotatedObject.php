<?php
/**
 * =============================================================================
 * Copyright (c) 2014-2015, Kirill Stepanov
 * All rights reserved.
 *
 * This file is part of MetaNotes and is licensed by the Copyright holder under
 * the 3-clause BSD License. The full text of the license can be found in the
 * LICENSE.txt file included in the root directory of this distribution or at
 * the link below.
 * =============================================================================
 *
 * @license http://opensource.org/licenses/BSD-3-Clause
 */

namespace MetaNotes;

use ReflectionObject;
use InvalidArgumentException;
use ArrayAccess,
    IteratorAggregate,
    Countable;

class AnnotatedObject
extends ReflectionObject
implements ArrayAccess, IteratorAggregate, Countable
{
    use AbstractAnnotatedClass;


    public function __construct($val)
    {
        if($val instanceof ReflectionObject)
        {
            throw new InvalidArgumentException('Cannot instantiate using a ' .
                'ReflectionObject: cannot access instance.');
        }
        else if(!is_object($val))
        {
            throw new InvalidArgumentException('Parameter is not a reflection or a class.');
        }

        parent::__construct($val);
        $this->setAnnotations($this->getDocComment());
        $this->setDefaultFactory();
    }
}
