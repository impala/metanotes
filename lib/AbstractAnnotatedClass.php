<?php
/**
 * =============================================================================
 * Copyright (c) 2014-2015, Kirill Stepanov
 * All rights reserved.
 *
 * This file is part of MetaNotes and is licensed by the Copyright holder under
 * the 3-clause BSD License. The full text of the license can be found in the
 * LICENSE.txt file included in the root directory of this distribution or at
 * the link below.
 * =============================================================================
 *
 * @license http://opensource.org/licenses/BSD-3-Clause
 */

namespace MetaNotes;

trait AbstractAnnotatedClass
{
    use Annotations;


    public function getConstructor()
    {
        $constructor_reflection = parent::getConstructor();
        return $constructor_reflection
            ? $this->_factory->getMethod($constructor_reflection)
            : null;
    }


    public function getMethod($name)
    {
        $method_reflection = parent::getMethod($name);
        return $this->_factory->getMethod($method_reflection);
    }


    public function getMethods($filter=0)
    {
        $output = array();
        // php being funky - doesn't like any value for default param
        $methods = $filter ? parent::getMethods($filter) : parent::getMethods();

        foreach($methods as $name => $reflection)
        {
            $output[$name] = $this->_factory->getMethod($reflection);
        }

        return $output;
    }


    public function getInterfaces()
    {
        $output = array();
        $interfaces = parent::getInterfaces();

        foreach($interfaces as $name => $reflection)
        {
            $output[$name] = $this->_factory->getClass($reflection);
        }

        return $output;
    }


    public function getTraits()
    {
        $output = array();
        $traits = parent::getTraits();

        foreach($traits as $name => $reflection)
        {
            $output[$name] = $this->_factory->getClass($name);
        }

        return $output;
    }


    public function getParentClass()
    {
        $parent_reflection = parent::getParentClass();
        return $parent_reflection
            ? $this->_factory->getClass($parent_reflection->name)
            : null;
    }


    public function getProperty($name)
    {
        return $this->_factory->getProperty($this->name, $name);
    }


    public function getProperties($filter=0)
    {
        $output = array();
        // see getMethods()
        $props = $filter ? parent::getProperties($filter) : parent::getProperties() ;

        foreach($props as $reflection)
        {
            $output[] = $this->_factory->getProperty($this->name, $reflection->name);
        }

        return $output;
    }
}
