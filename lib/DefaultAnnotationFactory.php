<?php
/**
 * =============================================================================
 * Copyright (c) 2014-2015, Kirill Stepanov
 * All rights reserved.
 *
 * This file is part of MetaNotes and is licensed by the Copyright holder under
 * the 3-clause BSD License. The full text of the license can be found in the
 * LICENSE.txt file included in the root directory of this distribution or at
 * the link below.
 * =============================================================================
 *
 * @license http://opensource.org/licenses/BSD-3-Clause
 */

namespace MetaNotes;


/**
 * Default factory for annotations. It blindly creates new instances from the
 * received arguments.
 */
class DefaultAnnotationFactory implements IAnnotationFactory
{
    public function getClass($class)
    {
        return new AnnotatedClass($class);
    }


    public function getFunction($function)
    {
        return new AnnotatedFunction($function);
    }


    public function getMethod($class, $method=null)
    {
        return new AnnotatedMethod($class, $method);
    }


    public function getObject($object)
    {
        return new AnnotatedObject($object);
    }


    public function getParameter($function, $parameter)
    {
        return new AnnotatedParameter($function, $parameter);
    }


    public function getProperty($class, $property)
    {
        return new AnnotatedProperty($class, $property);
    }
}
