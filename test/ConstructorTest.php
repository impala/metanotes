<?php
/**
 * =============================================================================
 * Copyright (c) 2014-2015, Kirill Stepanov
 * All rights reserved.
 *
 * This file is part of MetaNotes and is licensed by the Copyright holder under
 * the 3-clause BSD License. The full text of the license can be found in the
 * LICENSE.txt file included in the root directory of this distribution or at
 * the link below.
 * =============================================================================
 *
 * @license http://opensource.org/licenses/BSD-3-Clause
 */

use MetaNotes\AnnotatedClass as AClass,
    MetaNotes\AnnotatedObject as AObject,
    MetaNotes\AnnotatedFunction as AFunction,
    MetaNotes\AnnotatedMethod as AMethod,
    MetaNotes\AnnotatedProperty as AProperty,
    MetaNotes\AnnotatedParameter as AParameter;

class ConstructorTest extends PHPUnit_Framework_TestCase
{
    public function testClass()
    {
        $raw = new ReflectionClass('ArrayObject');
        $noted = new AClass($raw);
        $this->assertEquals($raw->name, $noted->name);

        $obj = new ArrayObject;
        $noted = new AClass($obj);
        $this->assertEquals('ArrayObject', $noted->name);

        try
        {
            new AClass(1337);
            $this->fail();
        }
        catch(InvalidArgumentException $e)
        {
            $this->assertStringStartsWith('Parameter is not', $e->getMessage());
        }

        try
        {
            new AClass('UnknownClass');
            $this->fail();
        }
        catch(InvalidArgumentException $e)
        {
            $this->assertStringStartsWith('Class(/interface/reflection)', $e->getMessage());
        }
    }


    public function testObject()
    {
        $noted = new AObject(new ArrayObject);
        $this->assertEquals('ArrayObject', $noted->name);

        // unfortunately this seems impossible to implement because there's no
        // way to extract the original object from the ReflectionObject instance
        try
        {
            $raw = new ReflectionObject(new ArrayObject);
            new AObject($raw);
            $this->fail();
        }
        catch(InvalidArgumentException $e)
        {
            $this->assertStringStartsWith('Cannot instantiate using', $e->getMessage());
        }

        try
        {
            new AObject('foobar');
            $this->fail();
        }
        catch(InvalidArgumentException $e)
        {
            $this->assertStringStartsWith('Parameter is not a reflection', $e->getMessage());
        }
    }


    public function testFunction()
    {
        $raw = new ReflectionFunction('print_r');
        $noted = new AFunction($raw);
        $this->assertEquals($raw->name, $noted->name);

        $noted = new AFunction('print_r');
        $this->assertEquals('print_r', $noted->name);

        try
        {
            $noted = new AFunction(42);
            $this->fail();
        }
        catch(InvalidArgumentException $e)
        {
            $this->assertStringStartsWith('Function 42', $e->getMessage());
        }
    }


    public function testMethod()
    {
        $raw = new ReflectionMethod(new ArrayObject, 'count');
        $noted = new AMethod($raw);
        $this->assertEquals($raw->name, $noted->name);
        $this->assertEquals($raw->class, $noted->class);

        $noted = new AMethod('ArrayObject::count');
        $this->assertEquals('count', $noted->name);
        $this->assertEquals('ArrayObject', $noted->class);

        $noted2 = new AMethod('ArrayObject', 'count');
        $this->assertEquals($noted2, $noted);
    }


    public function testProperty()
    {
        $raw = new ReflectionProperty('AnnotatedClass', 'foo1');
        $noted = new AProperty($raw);
        $this->assertEquals($raw->name, $noted->name);
        $this->assertEquals($raw->class, $noted->class);

        $noted = new AProperty('AnnotatedClass', 'foo1');
        $this->assertEquals('foo1', $noted->name);
        $this->assertEquals('AnnotatedClass', $noted->class);

        try
        {
            new AProperty('AnnotatedClass');
            $this->fail();
        }
        catch(InvalidArgumentException $e)
        {
            $this->assertStringStartsWith('Please provide', $e->getMessage());
        }
    }


    public function testParameter()
    {
        $raw = new ReflectionParameter('print_r', 0);
        $noted = new AParameter($raw);
        $this->assertEquals($raw->name, $noted->name);
        $this->assertNull($noted->getDeclaringClass());

        $raw = new ReflectionParameter(['AnnotatedClass', 'foo1'], 0);
        $noted = new AParameter($raw);
        $this->assertEquals($raw->name, $noted->name);
        $this->assertNotNull($noted->getDeclaringClass());

        $noted = new AParameter(['AnnotatedClass', 'foo1'], 0);
        $this->assertEquals($raw->name, $noted->name);

        try
        {
            new AParameter('moo');
            $this->fail();
        }
        catch(InvalidArgumentException $e)
        {
            $this->assertStringStartsWith('Parameter is not', $e->getMessage());
        }
    }
}
