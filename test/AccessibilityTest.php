<?php
/**
 * =============================================================================
 * Copyright (c) 2014-2015, Kirill Stepanov
 * All rights reserved.
 *
 * This file is part of MetaNotes and is licensed by the Copyright holder under
 * the 3-clause BSD License. The full text of the license can be found in the
 * LICENSE.txt file included in the root directory of this distribution or at
 * the link below.
 * =============================================================================
 *
 * @license http://opensource.org/licenses/BSD-3-Clause
 */

class AccessibilityTest extends PHPUnit_Framework_TestCase
{
    protected function setUp()
    {
        $this->reflection = new MetaNotes\AnnotatedClass('AnnotatedClass');
    }


    public function testClass()
    {
        $this->assertTrue($this->reflection->hasDocString());
        $this->assertEquals('bar_class', $this->reflection['foo_class']);
        $this->assertEquals(2, count($this->reflection));
    }


    public function testInstance()
    {
        $instance = $this->reflection->newInstance();
        $reflection = new MetaNotes\AnnotatedObject($instance);
        $this->assertEquals($reflection->asArray(), $this->reflection->asArray());
        $this->assertEquals($reflection->getMethod('foo1'), $this->reflection->getMethod('foo1'));
        $this->assertEquals($reflection->getProperty('foo1'), $this->reflection->getProperty('foo1'));
        $this->assertEquals($reflection->getConstructor(), $this->reflection->getConstructor());
    }


    public function testMethod()
    {
        $reflection = $this->reflection->getMethod('foo1');
        $this->assertInstanceOf('MetaNotes\\AnnotatedMethod', $reflection);
        $this->assertTrue($reflection->hasDocString());
        $this->assertEquals('bar1_method', $reflection['foo1_method']);
        $this->assertEquals(1, count($reflection));

        $reflection = $this->reflection->getMethod('foo2');
        $this->assertEquals('bar2_method', $reflection['foo2_method']);

        $reflection = $this->reflection->getMethod('foo3');
        $this->assertEquals('bar3_method', $reflection['foo3_method']);
    }


    public function testConstructor()
    {
        $reflection = $this->reflection->getConstructor();
        $this->assertInstanceOf('MetaNotes\\AnnotatedMethod', $reflection);
        $this->assertTrue($reflection->hasDocString());
        $this->assertEquals('bar_construct', $reflection['foo_construct']);
        $this->assertEquals(1, count($reflection));
    }


    public function testProperty()
    {
        $reflection = $this->reflection->getProperty('foo1');
        $this->assertInstanceOf('MetaNotes\\AnnotatedProperty', $reflection);
        $this->assertTrue($reflection->hasDocString());
        $this->assertEquals('bar1_prop', $reflection['foo1_prop']);
        $this->assertEquals(1, count($reflection));

        $reflection = $this->reflection->getProperty('foo2');
        $this->assertEquals('bar2_prop', $reflection['foo2_prop']);

        $reflection = $this->reflection->getProperty('foo3');
        $this->assertEquals('bar3_prop', $reflection['foo3_prop']);
    }


    public function testUndocumented()
    {
        $reflection = $this->reflection->getMethod('nonDocString');
        $this->assertFalse($reflection->hasDocString());
        $this->assertEquals(0, count($reflection));
    }


    public function testFunction()
    {
        $reflection = new MetaNotes\AnnotatedFunction('herp_derp');
        $this->assertEquals('bar_func', $reflection['foo_func']);
        $this->assertEquals(1, count($reflection));
        $this->assertEquals(null, $reflection->getClosureScopeClass());
    }


    public function testArrayAccess()
    {
        $this->assertEquals('bar_class', $this->reflection['foo_class']);
        $this->assertTrue(isset($this->reflection['foo_class']));
        $this->assertFalse(isset($this->reflection['moo']));

        try
        {
            $this->reflection['foo'] = 'bar';
            $this->fail();
        }
        catch(LogicException $e)
        {
            $this->assertStringStartsWith('Annotations are immutable', $e->getMessage());
        }

        try
        {
            unset($this->reflection['foo']);
        }
        catch(LogicException $e)
        {
            $this->assertStringStartsWith('Annotations are immutable', $e->getMessage());
        }
    }


    public function testIterator()
    {
        $tmp = array();

        foreach($this->reflection as $k => $v)
        {
            $tmp[$k] = $v;
        }

        $this->assertEquals($this->reflection->asArray(), $tmp);
    }
}
