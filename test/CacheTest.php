<?php
/**
 * =============================================================================
 * Copyright (c) 2014-2015, Kirill Stepanov
 * All rights reserved.
 *
 * This file is part of MetaNotes and is licensed by the Copyright holder under
 * the 3-clause BSD License. The full text of the license can be found in the
 * LICENSE.txt file included in the root directory of this distribution or at
 * the link below.
 * =============================================================================
 *
 * @license http://opensource.org/licenses/BSD-3-Clause
 */

class CacheTest extends PHPUnit_Framework_TestCase
{
    protected function setUp()
    {
        $this->cache = new MetaNotes\CachedAnnotationFactory;
    }


    public function testClass()
    {
        $obj1 = $this->cache->getClass('AnnotatedClass');
        $obj2 = $this->cache->getClass($obj1);

        $this->assertSame($obj1, $obj2);
    }


    public function testFunction()
    {
        $obj1 = $this->cache->getFunction('herp_derp');
        $obj2 = $this->cache->getFunction($obj1);

        $this->assertSame($obj1, $obj2);
    }


    public function testClosure()
    {
        $obj1 = $this->cache->getFunction(function() {});
        $obj2 = $this->cache->getFunction(function() {});

        // closures aren't cached because they can't be serialized
        // so objects aren't the same
        $this->assertEquals($obj1, $obj2);
    }


    public function testFunctionParameter()
    {
        $obj1 = $this->cache->getParameter('herp_derp', 'param1');
        $obj2 = $this->cache->getParameter($obj1->getDeclaringFunction(), 'param1');

        $this->assertSame($obj1, $obj2);
    }


    public function testMethodParameter()
    {
        $obj1 = $this->cache->getParameter(['AnnotatedClass', 'foo1'], 'e');
        $obj2 = $this->cache->getParameter($obj1->getDeclaringFunction(), 'e');

        $this->assertSame($obj1, $obj2);
    }


    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidParameter()
    {
        $this->cache->getParameter(0, 1);
    }
}
