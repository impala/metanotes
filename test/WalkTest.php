<?php
/**
 * =============================================================================
 * Copyright (c) 2014-2015, Kirill Stepanov
 * All rights reserved.
 *
 * This file is part of MetaNotes and is licensed by the Copyright holder under
 * the 3-clause BSD License. The full text of the license can be found in the
 * LICENSE.txt file included in the root directory of this distribution or at
 * the link below.
 * =============================================================================
 *
 * @license http://opensource.org/licenses/BSD-3-Clause
 */

class WalkTest extends PHPUnit_Framework_TestCase
{
    protected function setUp()
    {
        $this->reflection = new MetaNotes\AnnotatedObject(new AnnotatedClass);
        $this->child_reflection = new MetaNotes\AnnotatedClass('AnnotatedClass2');
    }


    public function testClassWalk()
    {
        $method = $this->reflection->getMethod('foo1');
        $this->assertInstanceOf('MetaNotes\\AnnotatedMethod', $method);
        $this->assertEquals('bar1_method', $method['foo1_method']);

        $params = $method->getParameters();
        $this->assertEquals(1, count($params));
        $param = $params[0];
        $this->assertInstanceOf('MetaNotes\\AnnotatedParameter', $param);
        $this->assertFalse($param->hasDocString());
        $method2 = $param->getDeclaringFunction();
        $this->assertEquals($method, $method2);

        $class = $param->getClass();
        $this->assertInstanceOf('MetaNotes\\AnnotatedClass', $class);
        $this->assertEquals('Exception', $class->name);

        $class = $param->getDeclaringClass();
        $this->assertInstanceOf('MetaNotes\\AnnotatedClass', $class);
        $this->assertEquals($this->reflection->asArray(), $class->asArray());

        $class = $method->getDeclaringClass();
        $this->assertInstanceOf('MetaNotes\\AnnotatedClass', $class);
        $this->assertEquals($this->reflection->asArray(), $class->asArray());

        $constructor = $class->getConstructor();
        $this->assertInstanceOf('MetaNotes\\AnnotatedMethod', $constructor);
        $this->assertEquals($this->reflection->getConstructor(), $constructor);
        $this->assertEquals('bar_construct', $constructor['foo_construct']);

        $prototype = $method->getPrototype();
        $this->assertInstanceOf('MetaNotes\\AnnotatedMethod', $prototype);
        $this->assertEquals('bar_abstract', $prototype['foo_abstract']);

        $class = $constructor->getDeclaringClass();
        $this->assertInstanceOf('MetaNotes\\AnnotatedClass', $class);
        $this->assertEquals($this->reflection->asArray(), $class->asArray());

        $property = $class->getProperty('foo3');
        $this->assertInstanceOf('MetaNotes\\AnnotatedProperty', $property);
        $this->assertEquals('bar3_prop', $property['foo3_prop']);

        $class = $property->getDeclaringClass();
        $this->assertInstanceOf('MetaNotes\\AnnotatedClass', $class);
        $this->assertEquals($this->reflection->asArray(), $class->asArray());

        $interfaces = $class->getInterfaces();
        $this->assertEquals(1, count($interfaces));
        $interface = $interfaces['Foo'];
        $this->assertInstanceOf('MetaNotes\\AnnotatedClass', $interface);
        $this->assertEquals('bar_interface', $interface['foo_interface']);

        $interface2 = new MetaNotes\AnnotatedClass('Foo');
        $this->assertEquals($interface, $interface2);

        $traits = $this->child_reflection->getTraits();
        $this->assertEquals(1, count($traits));
        $trait = $traits['AnnotatedTrait'];
        $this->assertInstanceOf('MetaNotes\\AnnotatedClass', $trait);
        $this->assertEquals('bar_trait', $trait['foo_trait']);
        $this->assertEquals($trait->getMethod('x')->asArray(), $this->child_reflection->getMethod('x')->asArray());
        $this->assertEquals('bar_trait_method', $trait->getMethod('x')['foo_trait_method']);

        $trait2 = new MetaNotes\AnnotatedClass('AnnotatedTrait');
        $this->assertEquals($trait, $trait2);
    }


    public function testFunctionWalk()
    {
        $func = new MetaNotes\AnnotatedFunction('herp_derp');
        $this->assertEquals('herp_derp', $func->name);

        $params = $func->getParameters();
        $this->assertEquals(2, count($params));
        $param = $params[1];

        $this->assertEquals('param2', $param->name);
        $func2 = $param->getDeclaringFunction();
        $this->assertEquals($func, $func2);

        $this->assertNull($param->getClass());
        $this->assertNull($param->getDeclaringClass());
    }


    public function testSubclass()
    {
        $this->assertEquals('bar_class2', $this->child_reflection['foo_class2']);

        $parent = $this->child_reflection->getParentClass();
        $this->assertEquals($this->reflection->asArray(), $parent->asArray());

        $parent_parent = $parent->getParentClass();
        $this->assertNull($parent_parent);
    }


    public function testMulti()
    {
        $methods = $this->reflection->getMethods();
        $this->assertEquals(5, count($methods));

        foreach($methods as $method)
        {
            $this->assertInstanceOf('MetaNotes\\AnnotatedMethod', $method);
        }

        $properties = $this->reflection->getProperties();
        $this->assertEquals(4, count($properties));

        foreach($properties as $property)
        {
            $this->assertInstanceOf('MetaNotes\\AnnotatedProperty', $property);
        }
    }


    public function testNoConstructor()
    {
        $reflection = new MetaNotes\AnnotatedClass('Foo');
        $this->assertNull($reflection->getConstructor());
    }
}
