<?php
/**
 * =============================================================================
 * Copyright (c) 2014-2015, Kirill Stepanov
 * All rights reserved.
 *
 * This file is part of MetaNotes and is licensed by the Copyright holder under
 * the 3-clause BSD License. The full text of the license can be found in the
 * LICENSE.txt file included in the root directory of this distribution or at
 * the link below.
 * =============================================================================
 *
 * @license http://opensource.org/licenses/BSD-3-Clause
 */


/**
 * @foo_interface bar_interface
 */
interface Foo
{
    /**
     * @foo_abstract bar_abstract
     */
    public function foo1(Exception $e);
}

/**
 * @foo_class bar_class
 * @meep meep
 */
class AnnotatedClass implements Foo
{
    /**
     * @foo1_prop bar1_prop
     */
    public $foo1 = 'bar1';


    /**
     * @foo2_prop bar2_prop
     */
    public $foo2 = 'bar2';


    /**
     * @foo3_prop bar3_prop
     */
    public $foo3 = 'bar3';


    /*
     * @x y
     */
    public $nonDocString;


    /**
     * @foo_construct bar_construct
     */
    public function __construct()
    {
    }


    /**
     * @foo1_method bar1_method
     */
    public function foo1(Exception $e)
    {
    }


    /**
     * @foo2_method bar2_method
     */
    protected function foo2()
    {
    }


    /**
     * @foo3_method bar3_method
     */
    private function foo3()
    {
    }


    /*
     * @x y
     */
    public function nonDocString()
    {
    }
}


/**
 * @foo_class2 bar_class2
 */
class AnnotatedClass2 extends AnnotatedClass { use AnnotatedTrait; }


/**
 * @foo_trait bar_trait
 */
trait AnnotatedTrait
{
    /**
     * @foo_trait_method bar_trait_method
     */
    public function x()
    {
    }
}
