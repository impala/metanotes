<?php
/**
 * =============================================================================
 * Copyright (c) 2014-2015, Kirill Stepanov
 * All rights reserved.
 *
 * This file is part of MetaNotes and is licensed by the Copyright holder under
 * the 3-clause BSD License. The full text of the license can be found in the
 * LICENSE.txt file included in the root directory of this distribution or at
 * the link below.
 * =============================================================================
 *
 * @license http://opensource.org/licenses/BSD-3-Clause
 */

ini_set('display_errors', 1);
error_reporting(E_ALL);

define('LIB', __DIR__ . '/../lib/');

require __DIR__ . '/AnnotatedClass.php';
require __DIR__ . '/annotated_function.php';

spl_autoload_register(function($class) {
    if(strpos($class, 'MetaNotes') === 0)
    {
        list($ns, $class) = explode('\\', $class, 2);
        require LIB . $class . '.php';
    }
});
