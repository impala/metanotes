<?php
/**
 * =============================================================================
 * Copyright (c) 2014-2015, Kirill Stepanov
 * All rights reserved.
 *
 * This file is part of MetaNotes and is licensed by the Copyright holder under
 * the 3-clause BSD License. The full text of the license can be found in the
 * LICENSE.txt file included in the root directory of this distribution or at
 * the link below.
 * =============================================================================
 *
 * @license http://opensource.org/licenses/BSD-3-Clause
 */

use MetaNotes\CachedAnnotationFactory,
    MetaNotes\DefaultAnnotationFactory;

class AnnotationCachePerformanceTest extends PHPUnit_Framework_TestCase
{
    private $timer;
    private $times = 1000;
    private $factor = 10;


    protected function setUp()
    {
        $this->cached_factory = new CachedAnnotationFactory;
        $this->default_factory = new DefaultAnnotationFactory;
    }


    public function testClass()
    {
        $t1 = $this->performNTimes(function() {
            $class = $this->cached_factory->getClass('AnnotatedClass2');
        }, $this->times);

        $t2 = $this->performNTimes(function() {
            $class = $this->default_factory->getClass('AnnotatedClass2');
        }, $this->times);

        $this->assertTrue($this->factor * $t1 < $t2, 'Cache is slower than direct usage.');
    }


    public function testForObject()
    {
        $obj = new stdClass;

        $t1 = $this->performNTimes(function() use ($obj) {
            $refl = $this->cached_factory->getObject($obj);
        }, $this->times);

        $t2 = $this->performNTimes(function() use ($obj) {
            $refl = $this->default_factory->getObject($obj);
        }, $this->times);

        $this->assertTrue($t1 < $t2, 'Cache is slower than direct usage.');
    }


    public function testForFunction()
    {
        $t1 = $this->performNTimes(function() {
            $refl = $this->cached_factory->getFunction('herp_derp');
        }, $this->times);

        $t2 = $this->performNTimes(function() {
                $refl = $this->default_factory->getFunction('herp_derp');
        }, $this->times);

        $this->assertTrue($this->factor * $t1 < $t2, 'Cache is slower than direct usage.');
    }


    public function testForMethod()
    {
        $t1 = $this->performNTimes(function() {
            $refl = $this->cached_factory->getMethod('AnnotatedClass', 'foo3');
        }, $this->times);

        $t2 = $this->performNTimes(function() {
            $refl = $this->default_factory->getMethod('AnnotatedClass', 'foo3');
        }, $this->times);

        $this->assertTrue($this->factor * $t1 < $t2, 'Cache is slower than direct usage.');
    }


    public function testForProperty()
    {
        $t1 = $this->performNTimes(function() {
            $refl = $this->cached_factory->getProperty('AnnotatedClass', 'foo3');
        }, $this->times);

        $t2 = $this->performNTimes(function() {
            $refl = $this->default_factory->getProperty('AnnotatedClass', 'foo3');
        }, $this->times);

        $this->assertTrue($this->factor * $t1 < $t2, 'Cache is slower than direct usage.');
    }


    public function testForParameter()
    {
        $t1 = $this->performNTimes(function() {
            $refl = $this->cached_factory->getParameter('herp_derp', 'param1');
        }, $this->times);

        $t2 = $this->performNTimes(function() {
            $refl = $this->default_factory->getParameter('herp_derp', 'param1');
        }, $this->times);

        $this->assertTrue($t1 < $t2, 'Cache is slower than direct usage.');
    }



    private function startTimer()
    {
        $this->timer = microtime(true);
    }


    private function stopTimer()
    {
        return microtime(true) - $this->timer;
    }


    private function performNTimes(Closure $f, $n=100)
    {
        $this->startTimer();

        for($i=0; $i<$n; $i++)
        {
            $f();
        }

        return $this->stopTimer();
    }
}
