PHP Annotations
===============

This library provides parsing for annotations defined in class, method and
member doc comments. Annotations are made available as an object with array
access capabilities.


Install
-------

Install via [Composer](http://getcomposer.org)

    {
        "require": {
            "metanotes/metanotes": "dev-master"
        }
    }


Instantiation
-------------

This library extends ~~all~~ most
[Reflection](http://www.php.net/manual/en/intro.reflection.php) classes to
provide support for annotations. To get annotations from a reflection, simply
use it as an array.

```php
<?php
use Metanotes\AnnotatedClass;

/**
 * @OneToMany Session
 */
class User
{
}

$class = new AnnotatedClass('User');
$class['onetomany'] === 'Session';
```

Annotation Syntax
-----------------

All annotations are **case insensitive**. The default value for all annotations
is `true`.

```php
<?php
/**
 * @Alive
 */
class User
{
}

$annotations = new AnnotatedClass('User');
$annotations['alive'] === true;
```

The absence of an annotation will result in a value of `null`:

```php
<?php
$annotations['zombie'] === null;
isset($annotations['zombie']) === false;
```

The `hasAnnotation($key)` method can be used to determine the existence of
an annotation, regardless of its value.

```php
<?php
$annotations->hasAnnotation('alive') === true;
$annotations->hasAnnotation('zombie') === false;
```


Annotation values
-----------------

A single annotation value can be defined as:

```php
<?php
/**
 * @LikesToEat cheese
 */
class Me extends User
{
}

$annotations = new AnnotatedClass('Me');
$annotations['LikesToEat'] === 'cheese';
```

Some values will be cast into their expected types; strings `'true'` and
`'false'` will be cast to their boolean equivalents and numeric values will be
cast to either int or float types.


#### Lists
List values can be specified by providing a comma separated
list surrounded with brackets:

```php
<?php
/**
 * @LikesToEat [ cheese, kraft dinner, hot dogs ]
 */
// ...

$annotations['LikesToEat'] == array('cheese', 'kraft dinner', 'hot dogs');
```

A parsed list value will be represented as a PHP array:

```php
<?php
is_array($annotations['LikesToEat']) === true
```


#### Maps

A map value can be specified using named parameters:

```php
<?php
/**
 * @LikesToEat weekend = [ chips, dip ], anytime = [ cheese, kraft dinner, hot dogs ]
 */
// ...

$annotations['likesToEat']['weekend'] == array('chips', 'dip');
$annotations['likesToEat']['anytime'] == array('cheese', 'kraft dinner', 'hot dogs');
```

* * *

**NOTE:** It is possible at this time to nest list values inside of another list or
to nest maps inside of another map or a list to use only json object start { and end with }.

```php
<?php
/** @MenuData {"root":{"child":{"arraychild":[0,1,2,3]}},"arraysroot":[1,2,3]} */
```

* * *
**NOTE:** All annotation values can be surrounded by optional parentheses.

```php
<?php
/**
 * @LikesToEat([ cheese, kraft dinner, hot dogs ])
 * @DoesNotLikeToEat( morning = salad, night = [ toast, fruit ])
 */
```
